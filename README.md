A small but useful shell script, which will grab all of the DNS entries for a chosen Route53 hosted zone, and try to establish a connection to a given port. Designed to test security group settings, and where you have access where you perhaps shouldn't.

Example usage: ./port-auditor.sh -d Z2ZM12345ABCDE -p 80

Requires: AWS cli (with relevant permissions to access Route53) and jq (for json parsing)